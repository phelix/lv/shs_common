﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Description" Type="Str">PHELIX SHS support library.

Copyright 2021 GSI Helmholtzzentrum für Schwerionenforschung GmbH

This software is released under GPLv3.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Ellipse" Type="Folder">
		<Item Name="Ellipse.lvclass" Type="LVClass" URL="../Ellipse/Ellipse.lvclass"/>
		<Item Name="EllipsePlotter.lvclass" Type="LVClass" URL="../EllipsePlotter/EllipsePlotter.lvclass"/>
	</Item>
	<Item Name="GradientField" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="GradientField.lvclass" Type="LVClass" URL="../GradientField/GradientField.lvclass"/>
		<Item Name="ShotAberrations.lvclass" Type="LVClass" URL="../GradientField/ShotAberrations/ShotAberrations.lvclass"/>
	</Item>
	<Item Name="Shifts" Type="Folder">
		<Item Name="Shifts.lvclass" Type="LVClass" URL="../Shifts/Shifts.lvclass"/>
		<Item Name="ShiftsAVG.lvclass" Type="LVClass" URL="../ShiftsAVG/ShiftsAVG.lvclass"/>
	</Item>
	<Item Name="WF Reconstructor" Type="Folder">
		<Item Name="WFReconstructor Harker-O&apos;Leary.lvclass" Type="LVClass" URL="../WFReconstructor/WFReconstructor Harker-O&apos;Leary/WFReconstructor Harker-O&apos;Leary.lvclass"/>
		<Item Name="WFReconstructor Linear.lvclass" Type="LVClass" URL="../WFReconstructor/WFReconstructor Linear/WFReconstructor Linear.lvclass"/>
		<Item Name="WFReconstructor Southwell.lvclass" Type="LVClass" URL="../WFReconstructor/WFReconstructor Southwell/WFReconstructor Southwell.lvclass"/>
		<Item Name="WFReconstructor Zernike.lvclass" Type="LVClass" URL="../WFReconstructor/WFReconstructor Zernike/WFReconstructor Zernike.lvclass"/>
		<Item Name="WFReconstructor Zou.lvclass" Type="LVClass" URL="../WFReconstructor/WFReconstructor Zou/WFReconstructor Zou.lvclass"/>
		<Item Name="WFReconstructor.lvclass" Type="LVClass" URL="../WFReconstructor/WFReconstructor.lvclass"/>
	</Item>
	<Item Name="WF2Img Converter" Type="Folder">
		<Item Name="WF2ImgConverter Farfield.lvclass" Type="LVClass" URL="../WF2ImgConverter/WF2ImgConverter Farfield/WF2ImgConverter Farfield.lvclass"/>
		<Item Name="WF2ImgConverter Interferogram.lvclass" Type="LVClass" URL="../WF2ImgConverter/WF2ImgConverter Interferogram/WF2ImgConverter Interferogram.lvclass"/>
		<Item Name="WF2ImgConverter Shearplate.lvclass" Type="LVClass" URL="../WF2ImgConverter/WF2ImgConverter Shearplate/WF2ImgConverter Shearplate.lvclass"/>
		<Item Name="WF2ImgConverter.lvclass" Type="LVClass" URL="../WF2ImgConverter/WF2ImgConverter.lvclass"/>
	</Item>
	<Item Name="Point.lvclass" Type="LVClass" URL="../Point/Point.lvclass"/>
	<Item Name="Rectangle.lvclass" Type="LVClass" URL="../Rectangle/Rectangle.lvclass"/>
	<Item Name="SHSReference.lvclass" Type="LVClass" URL="../SHSReference/SHSReference.lvclass"/>
	<Item Name="SHSResponse.lvclass" Type="LVClass" URL="../SHSResponse/SHSResponse.lvclass"/>
	<Item Name="Wavefront.lvclass" Type="LVClass" URL="../Wavefront/Wavefront.lvclass"/>
	<Item Name="Zernike.lvclass" Type="LVClass" URL="../Zernike/Zernike.lvclass"/>
</Library>
